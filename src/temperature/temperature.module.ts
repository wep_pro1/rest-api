import { Module } from '@nestjs/common';
import { TemperatrureController } from './temperature.controller';
import { TemperatureService } from './temperature.service';

@Module({
  imports: [],
  exports: [],
  controllers: [TemperatrureController],
  providers: [TemperatureService],
})
export class TemperatureModule {}
